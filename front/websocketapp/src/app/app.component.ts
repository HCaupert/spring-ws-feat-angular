import { Component } from '@angular/core';
import * as Stomp from 'stompjs';

import * as SockJS from 'sockjs-client';
import $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  serverUrl = 'http://localhost:8080/socket'
  title = 'WebSockets chat';
  stompClient;
  defaultUser = '{\"name\":\"Hugo\",\"age\":\"15\"}'

  constructor(){
    //this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection(serverUrl, channelUrl){
    let ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe(channelUrl, (message) => {
        if(message.body) {
          $(".chat").append("<div class='message'>"+message.body+"</div>")
          //console.log(message.body);
        }
      });
    });
  }

  sendMessage(message, messageAddress){
    this.stompClient.send(messageAddress , {}, message);
  }

  connectToServer(serverUrl){
    let ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    console.log(`connected to ${this.serverUrl}`)
  }

  subscribeToChannel(channelUrl){
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe(channelUrl, (message) => {
        if(message.body) {
          $(".chat").append("<div class='message'>"+message.body+"</div>")
          console.log(`received : ${message.body}`);
        }
      });
    });
  }
}
