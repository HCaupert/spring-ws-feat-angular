package web.socket.poc

import org.springframework.messaging.handler.annotation.DestinationVariable
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.stereotype.Controller
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.PathVariable
import java.text.SimpleDateFormat
import java.util.*

@Controller
class WebSocketController(val template: SimpMessagingTemplate) {

    @MessageMapping("/app/send/message/{id}")
    fun onReceivedMessage(user: User, @DestinationVariable id: Long){
        user.name = "${user.name} changed by Kotlin"
        template.convertAndSend("/chat/$id", user)
    }

}